Provenance
======

This lets you record provenance meta-data in generated homes. It adds
a `.provenance` file, as well `.channels.scm` and `.home.scm` files
when applicable.

These files allow you to recreate your home at a later point in time:

```bash
guix time-machine --channels=.channels.scm -- home build .home.scm
```

Main Configuration
------------------

Provenance meta-data is automatically generated by using the
`provenance-home-type` service type. 

**Scheme Procedure**: provenance-home-type

The type of service that generates provenance files for your generated
home. Its value is a location object.

**Scheme Procedure**: (home-with-provenance h [configuration-file])

Return a variant of _h_ that stores its own provenance information,
including _configuration-file_, if available. When
_configuration-file_ is omitted, the location where _h_ is defined is
used.

Example Configuration
---------------------

```scheme
(home-with-provenance
  (home
    (data-directory "/data/alice")
    (configurations
      (list
        (user-home something-home-type ...)
        ...))))
```


